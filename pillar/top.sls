base:
  # pillar applies to all hosts in the "base" env, our only env. We don't do
  # branch-based envs, because that actually sucks
  '*':
    - users  # looks for users.sls in root, or users/init.sls (like it's a Python pkg) 
    - ip_addrs

