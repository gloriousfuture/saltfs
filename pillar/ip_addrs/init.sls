
mine_functions:
  network.ip_addrs: 
    interface: eth0
  grains.item:
    - do_private_ip
    - do_public_ip
