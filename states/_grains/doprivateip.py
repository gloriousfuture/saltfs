#!/usr/bin/python


# NOTE(cm) Don't know how to deploy custom grain code yet.
# Instead, we accomplish this in state files with code like this:
#
# do_private_ip:
#   grains.present:
#     - value: {{ salt['cmd.run']("curl -s http://169.254.169.254/metadata/v1/interfaces/private/0/ipv4/address") }}



import urllib2

def _do_ip_addrs():
    """
    Fetch DigitalOcean public and private IPs and set them in custom grains.
    """

    public_ip = _request("http://169.254.169.254/metadata/v1/interfaces/public/0/ipv4/address")
    private_ip = _request("http://169.254.169.254/metadata/v1/interfaces/private/0/ipv4/address")
    return { 'do_private_ip': private_ip, 'do_public_ip': public_ip }

def _request(url):
    req = urllib2.Request(url)
    resp = urllib2.urlopen(req)
    return resp.read()

if __name__ == "__main__":
    print _do_ip_addrs()
