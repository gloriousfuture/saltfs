

# Do initial config
#
initial consul install:
  salt.state:
    - tgt: 'consul*'
    - highstate: True


# Do peers
# We MUST be invoked with pillar data that has our IP addresses
configure peers.hcl file:
  salt.state:
    - tgt: 'consul*'
    - sls:
      - consul.peers

# Do service
#
start and enable service:
  salt.state:
    - tgt: 'consul*'
    - sls:
      - consul.service


