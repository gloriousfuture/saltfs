
include:
  - digitalocean.ip_addrs
  - consul.dir
  - consul.binary
  - consul.server


Create consul user and group dude:
  user.present:
    - name: consul
    - empty_password: True
  
/etc/systemd/system/consul.service:
  file.managed:
    - source: salt://files/consul/consul.service
    - user: root
    - group: root


