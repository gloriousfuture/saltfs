
/opt/consul/consul.json:
  file.managed:
    - source: salt://files/consul/config.json
    - template: jinja
    - user: consul
    - group: consul
    - defaults:
      data_dir: /var/lib/consul
      is_server: true
      bootstrap_expect: 3

