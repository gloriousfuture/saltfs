
Create consul user and group dude:
  user.present:
    - name: consul
    - empty_password: True

include:
  - digitalocean.ip_addrs
  - consul.dir
  - consul.binary
  - consul.peers

  
/etc/systemd/system/consul.service:
  file.managed:
    - source: salt://files/consul/consul.service
    - user: root
    - group: root

/opt/consul/consul.json:
  file.managed:
    - source: salt://files/consul/config.json
    - template: jinja
    - user: consul
    - group: consul
    - defaults:
      data_dir: /var/lib/consul
      is_server: false
      bootstrap_expect: 0

Start and enable consul agent:
  service.running:
    - name: consul
    - enable: True

Restart consul agent:
  cmd.run:
    - name: systemctl restart consul

