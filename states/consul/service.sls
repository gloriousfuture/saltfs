

Start and enable consul service:
  service.running:
    - name: consul
    - enable: True

Restart consul:
  cmd.run:
    - name: systemctl restart consul

