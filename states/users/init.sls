

{% for username, item in pillar["users"].iteritems() %}

Create user {{ username }}:
  user.present:
    - name: {{ username }}
    - home: /home/{{ username }}
    - empty_password: True
    - groups:
      - wheel
  ssh_auth.present:
    - name: {{ item.ssh_public_key }}
    - user: {{ username }}
    - env: ssh-rsa

{% endfor %}
