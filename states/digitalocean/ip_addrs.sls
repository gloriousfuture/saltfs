
# These can be set into the salt mine

do_private_ip:
  grains.present:
    - value: {{ salt['cmd.run']("curl -s http://169.254.169.254/metadata/v1/interfaces/private/0/ipv4/address") }}


do_public_ip:
  grains.present:
    - value: {{ salt['cmd.run']("curl -s http://169.254.169.254/metadata/v1/interfaces/public/0/ipv4/address") }}


