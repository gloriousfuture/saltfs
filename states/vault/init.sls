
include:
  - vault.binary


Aw sheeyit bruh vault user:
  user.present:
    - name: vault
    - empty_password: True


/etc/systemd/system/vault.service:
  file.managed:
    - source: salt://files/vault/vault.service
    - user: root
    - group: root

/opt/vault:
  file.directory:
    - directory: true
    - makedirs: true
    - user: vault
    - group: vault

/opt/vault/vault.hcl:
  file.managed:
    - source: salt://files/vault/vault.hcl
    - template: jinja
    - user: vault
    - group: vault
    

Reload vault service:
  service.running:
    - name: vault
    - enable: True


# CAREFUL: restarts will SEAL the vault  

