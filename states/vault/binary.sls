
Install vault binary:
  archive.extracted:
    - name: /usr/local/bin
    - source: https://releases.hashicorp.com/vault/1.0.2/vault_1.0.2_linux_amd64.zip
    - skip_verify: true
    - enforce_toplevel: false
    - force: true

