# salt '*' state.apply
base:
  # status under '*' will apply to all minions
  'salt':
    - saltmaster
    - users
  # all consul servers
  'consul-*':
    - consul
  'nomad-server-*':
    - nomad
    - consul.agent
    - nomad.server
  'nomad-worker-*':
    - nomad
    - consul.agent
    - nomad.client
  'vault-*':
    - vault
    - consul.agent
    - users
  'coleman*':
    - users
