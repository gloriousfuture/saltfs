
Install nomad binary:
  archive.extracted:
    - name: /usr/local/bin
    - source: https://releases.hashicorp.com/nomad/0.8.7/nomad_0.8.7_linux_amd64.zip 
    - skip_verify: true
    - enforce_toplevel: false
    - force: true

