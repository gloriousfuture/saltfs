

include:
  - digitalocean.ip_addrs
  - nomad.binary
  - nomad.jobs
  - consul.binary

/opt/nomad:
  file.directory:
    - directory: true
    - makedirs: true

/etc/systemd/system/nomad.service:
  file.managed:
    - source: salt://files/nomad/nomad.service
    - user: root
    - group: root

