
/opt/nomad/jobs:
  file.directory:
    - directory: true
    - user: root
    - group: root

all nomad jobs:
  file.recurse:
    - name: /opt/nomad/jobs
    - source: salt://files/nomad/jobs

