
Start and enable nomad service:
  service.running:
    - name: nomad
    - enable: True

Restart nomad:
  cmd.run:
    - name: systemctl restart nomad

