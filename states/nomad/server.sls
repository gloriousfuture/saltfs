
Nomad server config:
  file.managed:
    - name: /opt/nomad/nomad.hcl
    - source: salt://files/nomad/server.hcl
    - user: root
    - group: root
    - template: jinja


include:
  - nomad.service
