
Nomad client config:
  file.managed:
    - name: /opt/nomad/nomad.hcl
    - source: salt://files/nomad/client.hcl
    - user: root
    - group: root
    - template: jinja

Required tools for nomad worker:
  pkg.installed:
    - pkgs:
      - git
      - curl
      - gcc

Job runner scripts:
  file.recurse:
    - name: /opt/nomad/bin
    - source: salt://files/nomad/jobs/bin

make runner scripts executable:
  cmd.run:
    - name: "chmod +x /opt/nomad/bin/*" 
  

include:
  - nomad.service
