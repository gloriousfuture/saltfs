# Here we lay down some helpful management scripts that wrap the salt command line
# for common tasks, and serve as documentation for how salt works.

# make a scripts directory in root's home
/root/scripts:
  file.directory:
    - directory: true
    - user: root
    - group: root

# copy all the contents of states/files/salt-master/scripts into /root/scripts

master management scripts:
  file.recurse:
    - name: /root/scripts
    - source: salt://files/saltmaster/scripts

