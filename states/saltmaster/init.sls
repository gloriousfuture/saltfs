
# Do not rely on ordering with includes, see: 
# https://docs.saltstack.com/en/latest/ref/states/include.html

include:
  - saltmaster.scripts


/usr/local/bin:
  archive.extracted:
    - source: https://github.com/digitalocean/doctl/releases/download/v1.12.2/doctl-1.12.2-linux-amd64.tar.gz
    - skip_verify: true
    - enforce_toplevel: false

# symlink a place on the default PATH
/usr/bin/doctl:
  file.symlink:
    # we expect this to be set right above
    - target: /usr/local/bin/doctl


# https://docs.saltstack.com/en/latest/ref/states/all/salt.states.pkg.html
Install some cool toolz on the master:
  pkg.installed:
    - pkgs:
      - jq
      - yum-cron

