
log_level = "DEBUG"

data_dir = "/tmp/nomad-client"

# Give the agent a unique name. Defaults to hostname
#name = "client1"

# Enable the client
client {
    enabled = true

    options = {
      "driver.raw_exec.enable" = "1"
    }

    # For demo assume we are talking to server1. For production,
    # this should be like "nomad.service.consul:4647" and a system
    # like Consul used for service discovery.
    servers = [
{% for min, ips in salt['mine.get']('nomad-server*', 'grains.item').iteritems() %}
  "{{ ips['do_private_ip'] }}",
{% endfor %}
    ]
}
