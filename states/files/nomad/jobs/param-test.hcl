
job "param-test" {

  datacenters = ["dc1"]

  type = "batch"

  meta {
    foo = "baz"
  }

  parameterized {
    meta_required = ["foo"]
  }

  task "take-params" {
    driver = "raw_exec"
    config {
      command = "/opt/nomad/jobs/bin/params.sh ${NOMAD_META_foo}"
    }
    resources {
      memory = 256

      network {
        mbits = 100
      }
    }
  }
}

