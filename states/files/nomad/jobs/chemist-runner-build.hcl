

job "chemist-runner-build" {

  datacenters = ["dc1"]

  type = "batch"

  periodic {
    cron             = "*/10 * * * * *"
    prohibit_overlap = true
  }

  task "run-build" {
    driver = "raw_exec"
    config {
      command = "/opt/nomad/bin/rust-build.sh"
    }
    artifact {
      source      = "git::https://gitlab.redox-os.org/coleman/chemist-runner.git"
      destination = "local/repo"
    }
    resources {
      cpu    = 50
      memory = 1024

      network {
        mbits = 100
      }
    }
  }
}

