job "mkosi-build" {
  datacenters = ["dc1"]
  type = "batch"
  periodic {
    cron             = "*/10 * * * * *"
    prohibit_overlap = true
  }

  task "run-mkosi-build" {
    driver = "raw_exec"
    config {
      command = "bash"
      args = ["local/repo/mkosi-build/run-mkosi-build.sh"]
    }
    artifact {
      source      = "git::https://gitlab.com/gloriousfuture/nomad-exec.git"
      destination = "local/repo"
    }
    resources {
      cpu    = 50
      memory = 256

      network {
        mbits = 100
      }
    }
  }
}

