
job "periodic-test" {

  datacenters = ["dc1"]
  type = "batch"

  periodic {
    cron             = "*/15 * * * * *"
    prohibit_overlap = true
  }
  task "just-log" {
    driver = "exec"
    config {
      command = "df"
      args = ["-h"]
    }
    resources {
      cpu    = 50
      memory = 256

      network {
        mbits = 100
      }
    }

  }
}

