


job "chemist-runner-build-chrooted" {

  datacenters = ["dc1"]

  type = "batch"

  periodic {
    cron             = "*/10 * * * * *"
    prohibit_overlap = true
  }

  task "run-build" {
    driver = "exec"
    config {
      command = "bash"
      args = ["local/bin/rust-build.sh"]
    }
    artifact {
      source      = "git::https://gitlab.com/gloriousfuture/nomad-exec.git"
      destination = "local/bin"
    }
    artifact {
      source      = "git::https://gitlab.redox-os.org/coleman/chemist-runner.git"
      destination = "local/repo"
    }
    resources {
      cpu    = 25
      memory = 1024

      network {
        mbits = 50
      }
    }
  }
}

