#!/bin/bash

curl https://sh.rustup.rs -sSf | sh -s -- -y --default-toolchain "nightly"

source $HOME/.cargo/env

echo "PWD: $(pwd)"
ls
cd local/repo
echo "REPO PWD: $(pwd)"
ls 
echo "CARGO CLEAN"
/root/.cargo/bin/cargo clean
ls

echo "CARGO BUILD"
/root/.cargo/bin/cargo build
ls
echo "TARGET/DEBUG DIR"
ls target/debug

echo "cargo binary???"
file /root/.cargo/bin/cargo

echo "DID THIS WORK"
ls

echo "THIS IS PATH: $PATH"

echo "let's run it:"
./target/debug/chemist-runner

sleep 3
echo "done"
