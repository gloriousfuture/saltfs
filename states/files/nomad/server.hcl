log_level = "DEBUG"
data_dir = "/tmp/nomad-server"

bind_addr = "{{ grains['do_private_ip'] }}"

server {
    enabled = true

    bootstrap_expect = 3
    server_join {
        retry_join = [ 
{% for min, ips in salt['mine.get']('nomad-server*', 'grains.item').iteritems() %}
  "{{ ips['do_private_ip'] }}",
{% endfor %}
        ]
        retry_max = 3
        retry_interval = "15s"
    }
}

# Disco Stu
advertise {
  # Defaults to the first private IP address.
  http = "{{ grains['do_private_ip'] }}"
  rpc  = "{{ grains['do_private_ip'] }}"
  serf = "{{ grains['do_private_ip'] }}" 
}
