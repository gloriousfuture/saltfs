
# we use HCL instaed of JSON because it lets us have trailing commas.
# This config is merged with others in the directory if we launch with the
# -config-dir parameter from the systemd unit file.

retry_join = [ 
{% for min, ips in salt['mine.get']('consul*', 'grains.item').iteritems() %}
  "{{ ips['do_private_ip'] }}",
{% endfor %}
]

