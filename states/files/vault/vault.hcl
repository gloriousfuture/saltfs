
listener "tcp" {
  address          = "0.0.0.0:8200"
  cluster_address  = "{{ grains['do_private_ip'] }}:8201"
  tls_disable      = "true"
}

storage "consul" {
  address = "127.0.0.1:8500"
  path    = "vault/"
}

# We do not need to set these if we have consul connecting us on the same LAN
# api_addr =  "$API_ADDR"
# cluster_addr = "$CLUSTER_ADDR"

