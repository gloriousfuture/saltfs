#!/bin/bash

# --inbound-rules string    comma-separated key:value list, example value: protocol:tcp,ports:22,droplet_id:1,droplet_id:2,tag:frontend, 
#   use quoted string of space-separated values for multiple rules

SSH_FROM_ANYWHERE="protocol:tcp,ports:22,address:0.0.0.0/0,address:::/0"

declare -a nomad_workers=$(doctl -t $AHELIUM_TOKEN compute droplet list "nomad-worker-*" --format ID --no-header)


for id in ${nomad_workers[@]}; do
    doctl -t $AHELIUM_TOKEN compute firewall create \
        --name "nomad-worker-$id-fw" \
        --inbound-rules "protocotl:tcp,ports:22,tag:internal"
done

