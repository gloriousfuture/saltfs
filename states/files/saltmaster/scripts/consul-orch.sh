#!/bin/bash

# We run a regular/highstate apply first, becuase we need the ip addrs set in
# this run, and it seems like we can't get mine values in one big orchestration
# run. So: apply first to lay down configs and set values, then update the mine,
# then run the orchestration routine which will _use_ the values when it
# configures the peers.hcl file. This is a bummer race condition.
salt 'consul*' state.apply consul
# we MUST bust the salt mine cache, because it has a long timeout
salt '*' mine.update
# run the orchestration routine in states/orch/consul_bootstrap/init.sls
salt-run state.orchestrate orch.consul_bootstrap

