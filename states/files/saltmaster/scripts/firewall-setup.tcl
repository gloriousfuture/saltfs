#!/usr/bin/env jimsh


set SSH_FROM_ANYWHERE "protocol:tcp,ports:22,address:0.0.0.0/0,address:::/0"

set tkn $env(AHELIUM_TOKEN)

set doctl_args [list -t $tkn compute droplet list --format "ID,Name" --no-header]

foreach { id name } [exec doctl {*}$doctl_args] {
    switch -glob $name {
        "nomad-worker-*" {
            puts "globbed a worker"
        }
    }
}

proc make_firewall { id name } {

}
